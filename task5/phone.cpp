#include <iostream>
using namespace std;

int main(){
  string tele;
  cout << "Enter the phone number: ";
  cin >> tele;

  string format = "(" + tele.substr(0,3) + ")" + " " + tele.substr(3,4)
  + " " + tele.substr(7,4);
  cout << "The formatted number is: " << format;
}
