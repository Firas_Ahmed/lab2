#include <iostream>
using namespace std;

int main(){
  float price, tip;
  cout << "Enter the price (dollars): ";
  cin >> price;
  cout << "Enter the tip (percentage): ";
  cin >> tip;

  float tip_mult = 1 + (tip/100);
  float total = price * (tip_mult);

  cout << "The tip is " << tip << "%\n";
  cout << "The total is $" << total;
}
